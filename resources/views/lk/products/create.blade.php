@extends('layouts.vertical', ['title' => 'Edit Products'])

@section('css')
    <!-- Plugins css -->
    <link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/summernote/summernote.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
        <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-lg-8">

                        </div>
                        <div class="col-lg-4">
                            <div class="text-lg-right mt-3 mt-lg-0">
                                <a href="{{route('lk.products.upload')}}" class="btn btn-danger waves-effect waves-light"><i class="mdi mdi-plus-circle mr-1"></i> Загрузить из файла</a>
                            </div>
                        </div><!-- end col-->
                    </div> <!-- end row -->
                </div> <!-- end card-box -->
            </div> <!-- end col-->
        </div>
        <!-- end row-->

        <form action="{{route('lk.products.store')}}" method="post" enctype="multipart/form-data" class="image_upload">
            @csrf

        <div class="row">
            <div class="col-lg-6">
                <div class="card-box">
                    <h5 class="text-uppercase bg-light p-2 mt-0 mb-3">General</h5>

                    <div class="form-group mb-3">
                        <label for="product-name">Наименование товара <span class="text-danger">*</span></label>
                        <input type="text" id="product-name" name="product-name" class="form-control" placeholder="e.g : Apple iMac">
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-name">Баркод <span class="text-danger">*</span></label>
                        <input type="text" id="product-barcode" name="product-barcode" class="form-control" placeholder="e.g : Apple iMac">
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-name">Артикул (sku) <span class="text-danger">*</span></label>
                        <input type="text" id="product-sku" name="product-sku" class="form-control" placeholder="e.g : Apple iMac">
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-reference">Модель <span class="text-danger">*</span></label>
                        <input type="text" id="product-model" name="product-model" class="form-control" placeholder="e.g : Apple iMac">
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-category">Страна производства <span class="text-danger">*</span></label>
                        <select class="form-control select2" id="product-region" name="product-region">
                            @foreach($regions as $region)
                                <option value="{{$region->id}}">{{$region->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-description">Product Description <span class="text-danger">*</span></label>
                        <textarea class="form-control" id="product-description" name="product-description" rows="5" placeholder="Please enter description"></textarea>
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-summary">Product Summary</label>
                        <textarea class="form-control" id="product-summary" name="product-summary" rows="3" placeholder="Please enter summary"></textarea>
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-category">Базовая категория <span class="text-danger">*</span>({{count($categories)}})</label>
                        <select class="form-control select2" id="product-base_category" name="product-base_category">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-category">Категория <span class="text-danger">*</span></label>
                        <select class="form-control select2" id="product-category" name="product-category">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-category">Сегмент <span class="text-danger">*</span>({{count($categories)}})</label>
                        <select class="form-control select2" id="product-segment" name="product-segment">
                            <option value="low-priced">Эконом</option>
                            <option value="middle-priced">Средний</option>
                            <option value="high-priced">Высокий</option>
                            <option value="luxury">Премиум</option>
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-base_price">Базовая оптовая цена <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="product-base_price" name="product-base_price" placeholder="Enter amount">
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-drop_price">Оптовая цена по дропшиппингу</label>
                        <input type="text" class="form-control" id="product-drop_price" name="product-drop_price" placeholder="Enter amount">
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-discount">Скидка</label>
                        <input type="text" class="form-control" id="product-discount" name="product-discount" placeholder="5% или 500">
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-price">РРЦ<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="product-price" name="product-price" placeholder="Enter amount">
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-min_order">Минимальный заказ (шт) </label>
                        <input type="text" class="form-control" id="product-min_order" name="product-min_order" placeholder="Enter amount">
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-category">Цвет <span class="text-danger">*</span></label>
                        <select class="form-control select2" id="product-color" name="product-color">
                            @foreach($colors as $color)
                                <option value="{{$color->id}}">{{$color->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label for="product-sizes">Размеры <span class="text-danger">*</span></label>
                        @foreach($sizes as $size)
                            <div class="form-group row mb-3">
                                <label class="col-form-label">{{$size->title}}</label>
                                <div class="col-sm-2">
                                    <input type="text" name="product-sizes[{{$size->id}}]" class="form-control" placeholder="1">
                                </div>
                                <label class="col-form-label">шт.</label>
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group mb-3">
                        <label class="mb-2">Модификация товара (Выберите товар) <span class="text-danger">*</span></label>
                        <select class="form-control select2" id="product-modification" name="product-modification">
                                <option value="0">Выберите базовый товар</option>
                            @foreach($products as $product)
                                <option value="{{$product->product_id}}">{{$product->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group mb-3">
                        <label class="mb-2">Статус <span class="text-danger">*</span></label>
                        <select class="form-control select2" id="product-status" name="product-status">
                            @foreach($statuses as $value=>$title)
                                <option value="{{$value}}">{{$title}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group mb-0">
                        <label>Comment</label>
                        <textarea class="form-control" rows="3" placeholder="Please enter comment"></textarea>
                    </div>
                </div> <!-- end card-box -->
            </div> <!-- end col -->

            <div class="col-lg-6">

                <div class="card-box">
                    <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Product Images</h5>

                    <input name="images[]" type="file" multiple />

                </div> <!-- end col-->

                <div class="card-box">
                    <h5 class="text-uppercase mt-0 mb-3 bg-light p-2">Meta Data</h5>

                    <div class="form-group mb-3">
                        <label for="product-meta-title">Meta title</label>
                        <input type="text" class="form-control" id="product-meta-title" placeholder="Enter title">
                    </div>

                    <div class="form-group mb-3">
                        <label for="product-meta-keywords">Meta Keywords</label>
                        <input type="text" class="form-control" id="product-meta-keywords" placeholder="Enter keywords">
                    </div>

                    <div class="form-group mb-0">
                        <label for="product-meta-description">Meta Description </label>
                        <textarea class="form-control" rows="5" id="product-meta-description" placeholder="Please enter description"></textarea>
                    </div>
                </div> <!-- end card-box -->

            </div> <!-- end col-->
        </div>
        <!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="text-center mb-3">
                    <button type="button" class="btn w-sm btn-light waves-effect">Cancel</button>
                    <button type="submit" class="btn w-sm btn-success waves-effect waves-light">Save</button>
                </div>
            </div> <!-- end col -->
        </div>
        <!-- end row -->

        </form>

        <!-- file preview template -->
        <div class="d-none" id="uploadPreviewTemplate">
            <div class="card mt-1 mb-0 shadow-none border">
                <div class="p-2">
                    <div class="row align-items-center">
                        <div class="col-auto">
                            <img data-dz-thumbnail src="#" class="avatar-sm rounded bg-light" alt="">
                        </div>
                        <div class="col pl-0">
                            <a href="javascript:void(0);" class="text-muted font-weight-bold" data-dz-name></a>
                            <p class="mb-0" data-dz-size></p>
                        </div>
                        <div class="col-auto">
                            <!-- Button -->
                            <a href="" class="btn btn-link btn-lg text-muted" data-dz-remove>
                                <i class="dripicons-cross"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div> <!-- container -->
@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{asset('assets/libs/summernote/summernote.min.js')}}"></script>
    <script src="{{asset('assets/libs/dropzone/dropzone.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/form-fileuploads.init.js')}}"></script>
    <script src="{{asset('assets/js/pages/add-product.init.js')}}"></script>
@endsection

