@extends('layouts.vertical', ['title' => 'Ecommerce Products'])


@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">eCommerce</a></li>
                            <li class="breadcrumb-item active">Products</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Products</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div ui-view="content" class="ng-scope"><div class="card__content ng-scope">

                <div class="btm-space-lg">
                    <mass-uploader only-img="false" max-file-size="30000000" limit="1" btn-default-text="Загрузить файл .XLSX" url="/my/json/company/54c6026d-bd46-4d3a-98e7-08cbe7f55317/xls_upload" on-upload="onUploaded" class="ng-isolate-scope"><div over-class="dropzone--active" class="dropzone text-center" nv-file-drop="" nv-file-over="" uploader="uploader">
                            <a href="" class="btn btn-primary ng-binding" data-ng-click="clickUpload()">
        <span class="icon-float icon-float--r-space">
            <i class="material-icons icon-float__icon icon-float__icon--md data-ng-hide ng-hide" data-ng-show="onlyImg">camera_alt</i>
            <i class="material-icons icon-float__icon icon-float__icon--md data-ng-hide" data-ng-show="!onlyImg">file_upload</i>
        </span>
                                Загрузить файл .XLSX
                                <span class="loading loading-white data-ng-hide ng-hide" data-ng-show="showLoad"></span>
                            </a>

                            <div class="text-danger ng-binding ng-hide" data-ng-show="errText" style="white-space: pre-wrap;"></div>
                            <div class="text-warning ng-binding ng-hide" data-ng-show="limitText"></div>
                            <input type="file" nv-file-select="" uploader="uploader" id="mass-uploader-btn" class="ng-hide" multiple="">
                        </div>

                    </mass-uploader>
                </div>

                <h3>Формат XLSX (Excel 2007-365)</h3>
                <p>Вы можете подготовить каталог товаров в программе Microsoft Excel и загрузить на наш сайт.</p>

                <p>
                    Файл должен полностью соответствовать нашему шаблону (см. ниже).
                    Также наш формат совместим с форматом
                    <a href="https://yandex.ru/support/partnermarket/export/excel-format.html" target="_blank">Яндекс.Маркет Excel</a>,
                    за исключением того что файл нужно сохранить в формате XLSX (меню «Сохранить как...» в Excel).
                </p>

                <h4>Шаг 1. Скачайте шаблон</h4>
                <p>
                    <a href="/static/doc/optlist_excel.xlsx" target="_blank" class="btn btn-default">Скачать шаблон</a>
                </p>

                <h4>Шаг 2. Добавьте в шаблон ваши товары</h4>
                <p>Первые четыре строки с товарами заполнены для примера. Отредактируйте их, указав свои товары.</p>
                <div class="clearfix">
                    <p><a href="" data-ng-click="showFieldsHelp=!showFieldsHelp" data-ng-init="showFieldsHelp=false">Описание полей</a></p>
                    <div class="ng-hide" data-ng-show="showFieldsHelp">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Поле</th>
                                <th>Описание</th>
                                <th>Обязательность</th>
                            </tr>
                            </thead>
                            <tbody><tr>
                                <td>id</td>
                                <td>Идентификатор товара. Может состоять только из цифр и латинских букв. Максимальная длина — 20 символов. Должен быть уникальным для каждого товара.</td>
                                <td>Обязательно</td>
                            </tr>
                            <tr>
                                <td>Статус товара</td>
                                <td>С помощью этого поля вы можете задать для товара надпись «на заказ».</td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Ссылка на товар на сайте магазина</td>
                                <td>URL-страницы товара на сайте магазина (если есть). Максимальная длина ссылки — 512 символов. URL-адрес формируется на основе стандарта <a href="https://tools.ietf.org/html/rfc3986" target="_blank">RFC 3986</a>.</td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Производитель</td>
                                <td>Название производителя.</td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Название</td>
                                <td>Полное название предложения, в которое входит: тип товара, производитель, модель и название товара, важные характеристики. Составляйте по схеме:
                                    <strong>что</strong> (тип товара) + <strong>кто</strong> (производитель) + <strong>товар</strong> (модель, название) + важные характеристики. </td>
                                <td>Обязательно</td>
                            </tr>
                            <tr>
                                <td>Категория</td>
                                <td>Название категории товара. Категории должны быть предварительно созданы на&nbsp;вкладке <a ui-sref="product.cat" href="#/product/cat">Категории</a></td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Цена</td>
                                <td>Актуальная цена товара. При 0 или отсутствии будет выводиться «По запросу».</td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Валюта</td>
                                <td>Валюта, в которой указана цена товара: RUR, USD, EUR, UAH, KZT, BYN. Цена и валюта должны соответствовать друг другу. Например, вместе с USD надо указывать цену в долларах, а не в рублях.</td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Ссылка на картинку</td>
                                <td>
                                    <p>URL-ссылка на картинку товара.</p>

                                    <p>Требования к ссылкам</p>

                                    <ul>
                                        <li>Допустимые протоколы URL изображения — HTTP или HTTPS.</li>
                                        <li>Ссылка на изображение должна быть абсолютной. Относительные ссылки недопустимы.</li>
                                        <li>По указанной ссылке должно размещаться изображение товара, соответствующего описанию
                                            предложения. Нельзя использовать изображение другого товара.
                                        </li>
                                        <li>Запрещено использовать ссылку на одно и то же изображение в описании разных товаров.
                                            Исключение — модификации одной модели, которые внешне не отличаются, например, Apple
                                            iPhone 6 Silver 16 GB и Apple iPhone 6 Silver 32 GB.
                                        </li>
                                        <li>Недопустима ссылка на HTML-страницу, содержащую изображение.</li>
                                        <li>Недопустима ссылка на логотип магазина или «заглушку» (страницу, где написано «картинка
                                            отсутствует» и т. п.).
                                        </li>
                                        <li>Максимальная длина URL — 512 символов.</li>
                                        <li>URL‑адрес формируется на основе стандарта <a href="https://tools.ietf.org/html/rfc3986" target="_blank">RFC 3986</a>.</li>
                                    </ul>


                                </td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Описание</td>
                                <td>
                                    <p>Описание предложения. Длина текста не более 3000 символов (включая знаки препинания).</p>
                                    <ul>
                                        <li>Номера телефонов, адреса электронной почты, почтовые адреса, номера ICQ, логины мессенджеров, любые ссылки.</li>
                                        <li>Слова «скидка», «распродажа», «дешевый», «подарок» (кроме подарочных категорий), «бесплатно», «акция», «специальная цена», «новинка», «new», «аналог», «заказ», «хит».</li>
                                        <li>Условия продажи товара, например, данные об акциях или предоплате (их нужно передавать в элементе sales_notes в YML / Условия продажи в XLS).</li>
                                        <li>Регион, в котором продается товар.</li>
                                        <li>Информацию о разных модификациях товара (например, нельзя писать «товар в ассортименте»). Для каждой модификации нужно создать отдельное предложение.</li>
                                    </ul>
                                </td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Характеристики товара</td>
                                <td>
                                    <p>Укажите параметры товара в формате:</p>

                                    <div class="well well-sm">
                                        параметр X | значение X | ед.измерения ;<br>
                                        параметр Y | значение Y | ед.измерения ;
                                    </div>

                                    <ul>
                                        <li>После каждого параметра поставьте точку с запятой.</li>
                                        <li>Название параметра, единицы измерения и значение разделите вертикальной чертой |. Обязательно указывать только название.</li>
                                        <li>Если у параметра нет единиц измерения или значения, не указывайте ничего.</li>
                                    </ul>

                                    <p>Примеры</p>

                                    <div class="well well-sm">
                                        Размер экрана|27|дюйм;Материал|алюминий;
                                    </div>
                                    <div class="well well-sm">
                                        Wi-Fi;
                                    </div>

                                </td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Минимальное количество</td>
                                <td>Минимальное количество для заказа в штуках. Только целое число.</td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Условия продажи</td>
                                <td>Можно указать варианты оплаты, необходимость предоплаты, акции и распродажи</td>
                                <td>Необязательно</td>
                            </tr>
                            <tr>
                                <td>Страна происхождения</td>
                                <td>Страна производства товара. </td>
                                <td>Необязательно</td>
                            </tr>
                            </tbody></table>

                    </div>
                </div>

                <h4>Шаг 3. Загрузите файл</h4>
                <p>Загрузте файл нажав на кнопку «Загрузить» вверху этой страницы или перетащив его в поле загрузки.</p>

                <h4>Дополнительная информация</h4>
                <p>Требования</p>
                <ul>
                    <li>Файл должен иметь расширение <strong>.xlsx</strong>, т.е.&nbsp;быть сохранен в&nbsp;формте Excel 2007-365</li>
                    <li>В шаблоне запрещено удалять строку с&nbsp;названиями полей</li>
                    <li>Товары должны располагаться на&nbsp;листе «Товары»</li>
                    <li>Если вы используете категории, они должны быть предварительно созданы на&nbsp;вкладке <a ui-sref="product.cat" href="#/product/cat">Категории</a></li>
                    <li>Один товар должен занимать только одну строку</li>
                    <li>По&nbsp;ссылкам на картинки должны быть доступны фото в&nbsp;открытом доступе</li>
                    <li>Для обновления товаров просто загрузите файл с&nbsp;id товаров, которые уже есть в&nbsp;базе</li>
                </ul>

            </div></div>

    </div> <!-- container -->
@endsection
