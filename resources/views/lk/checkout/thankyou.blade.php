@extends('layouts.vertical', ['title' => 'Checkout'])

@section('css')
    <!-- Plugins css -->
    <link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Ecommerce</a></li>
                            <li class="breadcrumb-item active">Checkout</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Checkout</h4>
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-lg-12">
                <div class="container">
                    <h1>Wonderful {{ $order->getBillpayer()->firstname }}!</h1>
                    <hr>

                    <div class="alert alert-success">Your order has been registered with number
                        <strong>{{ $order->getNumber() }}</strong>.
                    </div>

                    <h3>Next Steps</h3>

                    <ol>
                        <li>Your order will be prepared in the next 24 hours.</li>
                        <li>Your package will be handed over to the courier.</li>
                        <li>You'll receive an E-mail with the Shipment Information.</li>
                    </ol>

                    <div>
                        <a href="{{ route('lk.orders.index') }}" class="btn btn-info">All right!</a>
                    </div>

                </div>
            </div>
        </div>
        <!-- end row -->
        
    </div> <!-- container -->
@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>

    <script>
        $('[data-toggle="select2"]').select2();
        $('document').ready(function () {
            $('a#success_btn').click(function () {
                $form = $('#checkout_form');
                $form.submit();
            });
        });
    </script>

@endsection