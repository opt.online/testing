@extends('layouts.vertical', ['title' => 'Create Project'])

@section('css')
    <!-- Plugins css -->
    <link href="{{asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css" />

@endsection

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">UBold</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Projects</a></li>
                            <li class="breadcrumb-item active">Create Project</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Create Project</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <form action="{{route('lk.tenders.store')}}" method="post" id="cart_form">
                    @csrf
                    <div class="card">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-xl-6">
                                <div class="form-group mb-3">
                                    <label for="product-category">Категория <span class="text-danger">*</span></label>
                                    <select class="form-control select2-multiple" data-toggle="select2" id="product-category" multiple="multiple" name="category[]">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group mb-3">
                                    <label for="product-category">Сегмент <span class="text-danger">*</span></label>
                                    <select class="form-control select2-multiple" data-toggle="select2" id="product-segment" multiple="multiple" name="segment[]">
                                        <option value="low-priced">Эконом</option>
                                        <option value="middle-priced">Средний</option>
                                        <option value="high-priced">Высокий</option>
                                        <option value="luxury">Премиум</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="project-priority">Опт</label>

                                    <select class="form-control select2-multiple" data-toggle="select2" multiple="multiple" data-placeholder="Выберете оптовые объемы" name="opt[]">
                                        <option value="low">Мелкий</option>
                                        <option value="middle">Средний</option>
                                        <option value="high">Крупный</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="project-budget" class="min_summ_buy">Сумма закупки</label>
                                    <input type="text" id="project-budget" class="form-control" name="min_order" placeholder="Enter project budget">
                                </div>

                                <div class="form-group mb-3">
                                    <label for="product-category">Регион доставки<span class="text-danger">*</span></label>
                                    <select class="form-control select2" data-toggle="select2" id="delivery_cover" name="delivery_cover">
                                        @foreach($regions as $region)
                                            <option value="{{$region->id}}">{{$region->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="product-category">Размеры<span class="text-danger">*</span></label>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="all_sizes">
                                        <label class="custom-control-label" for="all_sizes">Все</label>
                                    </div>
                                    @foreach($sizes as $size)
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="size{{$size->id}}" name="sizes[]" value="{{$size->id}}">
                                            <label class="custom-control-label" for="size{{$size->id}}">{{$size->title}}</label>
                                        </div>
                                    @endforeach
                                </div>

                            </div> <!-- end col-->
                        </div>
                        <!-- end row -->


                        <div class="row mt-3">
                            <div class="col-12 text-center">
                                <button type="button" class="btn btn-success waves-effect waves-light m-1" id="create_tender"><i class="fe-check-circle mr-1"></i> Create</button>
                                <button type="button" class="btn btn-light waves-effect waves-light m-1"><i class="fe-x mr-1"></i> Cancel</button>
                            </div>
                        </div>

                    </div> <!-- end card-body -->
                </div> <!-- end card-->
                </form>
            </div> <!-- end col-->
        </div>
        <!-- end row-->

    </div> <!-- container -->
@endsection

@section('script')
    <!-- Plugins js-->
    <script src="{{asset('assets/libs/dropzone/dropzone.min.js')}}"></script>
    <script src="{{asset('assets/libs/select2/select2.min.js')}}"></script>
    <script src="{{asset('assets/libs/flatpickr/flatpickr.min.js')}}"></script>

    <!-- Page js-->
    <script src="{{asset('assets/js/pages/create-project.init.js')}}"></script>

    <script>
        $('document').ready(function () {
            $('button#create_tender').click(function () {
                $form = $('#cart_form');
                $form.submit();
            });
            $('input#all_sizes').on('click',function(){
                if(this.checked){
                    $('input[type="checkbox"]').each(function(){
                        this.checked = true;
                    })
                }
                else {
                    $('input[type="checkbox"]').each(function(){
                        this.checked = false;
                    })
                }
            });
        });
    </script>
@endsection
