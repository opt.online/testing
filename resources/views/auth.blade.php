
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.shared.title-meta', ['title' => "Log In"])

    @include('layouts.shared.head-css')
</head>

<body class="authentication-bg authentication-bg-pattern">

<!-- Begin page -->
<div id="app">
    <app></app>
</div>
<footer class="footer footer-alt">
    <script>document.write(new Date().getFullYear())</script> &copy; UBold theme by <a href="" class="text-white-50">Coderthemes</a>
</footer>

@include('layouts.shared.footer-script')
</body>
</html>
