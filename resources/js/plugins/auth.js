import {createAuth}          from '@websanova/vue-auth/src/v3.js';
import driverAuthBearer      from '@websanova/vue-auth/src/drivers/auth/bearer.js';
import driverHttpAxios       from '@websanova/vue-auth/src/drivers/http/axios.1.x.js';
import driverRouterVueRouter from '@websanova/vue-auth/src/drivers/router/vue-router.2.x.js';
import AuthComponent         from '../components/auth';

export default (app) => {
    app.use(createAuth({
        plugins: {
            http: app.axios,
            router: app.router,
        },
        drivers: {
            http: driverHttpAxios,
            auth: driverAuthBearer,
            router: driverRouterVueRouter,
            oauth2: {

            }
        },
        options: {
            auth: AuthComponent,
            http: driverHttpAxios,
            rolesKey: 'type',
            registerData:       {url: '/register',      method: 'POST', redirect: '/company/create',                  autoLogin: true           },
            loginData:          {url: '/login',         method: 'POST', redirect: '/dashboard',      fetchUser: false, staySignedIn: true         },
            logoutData:         {url: '/logout',        method: 'POST', redirect: '/',                       makeRequest: false         },
            fetchData:          {url: '/user',          method: 'GET',                                       enabled: true              },
            refreshData:        {url: '/refresh',       method: 'GET',                                       enabled: true, interval: 30},
            impersonateData:    {url: '/impersonate',   method: 'POST', redirect: '/',      fetchUser: true                             },
            unimpersonateData:  {url: '/unimpersonate', method: 'POST', redirect: '/admin', fetchUser: true, makeRequest: false         },
            oauth2Data:         {url: '/social',        method: 'POST', redirect: '/',      fetchUser:true                              },
            notFoundRedirect: {name: 'user-account'},
            authRedirect:       {path: '/dashboard'},
        }
    }));
}
