import { createWebHistory, createRouter } from "vue-router";

function loadView(view) {
    return () => import(`./views/pages/${view}.vue`);
}

const routes = [
    {
        path: "/",
        name: "Home",
        component: loadView('index'),
    },
    {
        path: "/login",
        name: "Login",
        component: loadView('auth/login')
    },
    {
        path: "/register",
        name: "Registration",
        component: loadView('auth/register')
    },
    {
        path: "/lk/company/create",
        name: "CompanyCreate",
        component: loadView('company/create'),
        meta:{
            auth: true
        }
    },
    {
        path: "/lk/products/create",
        name: "Product Create",
        component: loadView('products/create'),
    },
    {
        path: "/lk/products/upload",
        name: "Product Upload",
        component: loadView('products/upload'),
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default (app) => {
    app.router = router;

    app.use(router);
}

//export default router;
