<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Vanilo\Properties\Models\Property;

class PropertiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $color = Property::create(['name' => 'Цвет', 'slug'=>'color', 'type' => 'text']);
        $color->propertyValues()->createMany([
            ['title' => 'Черный'],
            ['title' => 'Белый'],
            ['title' => 'Желтый']
        ]);

        $sizes = Property::create(['name' => 'Размер', 'slug'=>'size', 'type' => 'text','configuration'=>'{"type":"Шт."}']);
        $sizes->propertyValues()->createMany([
            ['title' => '44', 'category_type'=>'mans-clothes'],
            ['title' => '46', 'category_type'=>'mans-clothes'],
            ['title' => '48', 'category_type'=>'mans-clothes'],
            ['title' => '50', 'category_type'=>'mans-clothes'],
            ['title' => '52', 'category_type'=>'mans-clothes'],
            ['title' => '54', 'category_type'=>'mans-clothes'],
            ['title' => '56', 'category_type'=>'mans-clothes'],
            ['title' => '58', 'category_type'=>'mans-clothes'],
            ['title' => 'S', 'category_type'=>'mans-clothes'],
            ['title' => 'M', 'category_type'=>'mans-clothes'],
            ['title' => 'L', 'category_type'=>'mans-clothes'],
            ['title' => 'XL', 'category_type'=>'mans-clothes'],
            ['title' => 'XXL', 'category_type'=>'mans-clothes'],
            ['title' => 'XXXL', 'category_type'=>'mans-clothes'],
        ]);

        $sizes->propertyValues()->createMany([
            ['title' => '42', 'category_type'=>'mans-jeans'],
            ['title' => '44', 'category_type'=>'mans-jeans'],
            ['title' => '46', 'category_type'=>'mans-jeans'],
            ['title' => '48', 'category_type'=>'mans-jeans'],
            ['title' => '50', 'category_type'=>'mans-jeans'],
            ['title' => '52', 'category_type'=>'mans-jeans'],
            ['title' => '54', 'category_type'=>'mans-jeans'],
            ['title' => '56', 'category_type'=>'mans-jeans'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '44', 'category_type'=>'mans-outerwear'],
            ['title' => '46', 'category_type'=>'mans-outerwear'],
            ['title' => '48', 'category_type'=>'mans-outerwear'],
            ['title' => '50', 'category_type'=>'mans-outerwear'],
            ['title' => '52', 'category_type'=>'mans-outerwear'],
            ['title' => '54', 'category_type'=>'mans-outerwear'],
            ['title' => '56', 'category_type'=>'mans-outerwear'],
            ['title' => '58', 'category_type'=>'mans-outerwear'],
            ['title' => '60', 'category_type'=>'mans-outerwear'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '46', 'category_type'=>'mans-jacket'],
            ['title' => '48', 'category_type'=>'mans-jacket'],
            ['title' => '50', 'category_type'=>'mans-jacket'],
            ['title' => '52', 'category_type'=>'mans-jacket'],
            ['title' => '54', 'category_type'=>'mans-jacket'],
            ['title' => '56', 'category_type'=>'mans-jacket'],
            ['title' => '58', 'category_type'=>'mans-jacket'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '36', 'category_type'=>'mans-shirts'],
            ['title' => '37', 'category_type'=>'mans-shirts'],
            ['title' => '38', 'category_type'=>'mans-shirts'],
            ['title' => '39', 'category_type'=>'mans-shirts'],
            ['title' => '40', 'category_type'=>'mans-shirts'],
            ['title' => '41', 'category_type'=>'mans-shirts'],
            ['title' => '42', 'category_type'=>'mans-shirts'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '40', 'category_type'=>'mans-tshorts'],
            ['title' => '42', 'category_type'=>'mans-tshorts'],
            ['title' => '44', 'category_type'=>'mans-tshorts'],
            ['title' => '46', 'category_type'=>'mans-tshorts'],
            ['title' => '48', 'category_type'=>'mans-tshorts'],
            ['title' => '50', 'category_type'=>'mans-tshorts'],
            ['title' => '52', 'category_type'=>'mans-tshorts'],
            ['title' => '54', 'category_type'=>'mans-tshorts'],
            ['title' => '56', 'category_type'=>'mans-tshorts'],
            ['title' => '58', 'category_type'=>'mans-tshorts'],
            ['title' => 'XS', 'category_type'=>'mans-tshorts'],
            ['title' => 'S', 'category_type'=>'mans-tshorts'],
            ['title' => 'M', 'category_type'=>'mans-tshorts'],
            ['title' => 'L', 'category_type'=>'mans-tshorts'],
            ['title' => 'XL', 'category_type'=>'mans-tshorts'],
            ['title' => 'XXL', 'category_type'=>'mans-tshorts'],
            ['title' => 'XXXL', 'category_type'=>'mans-tshorts'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '44', 'category_type'=>'mans-underwear'],
            ['title' => '46', 'category_type'=>'mans-underwear'],
            ['title' => '48', 'category_type'=>'mans-underwear'],
            ['title' => '50', 'category_type'=>'mans-underwear'],
            ['title' => '52', 'category_type'=>'mans-underwear'],
            ['title' => '54', 'category_type'=>'mans-underwear'],
            ['title' => '56', 'category_type'=>'mans-underwear'],
            ['title' => 'XS', 'category_type'=>'mans-underwear'],
            ['title' => 'S', 'category_type'=>'mans-underwear'],
            ['title' => 'M', 'category_type'=>'mans-underwear'],
            ['title' => 'L', 'category_type'=>'mans-underwear'],
            ['title' => 'XL', 'category_type'=>'mans-underwear'],
            ['title' => 'XXL', 'category_type'=>'mans-underwear'],
            ['title' => 'XXXL', 'category_type'=>'mans-underwear'],
        ]);

        $sizes->propertyValues()->createMany([
            ['title' => '23', 'category_type'=>'mans-socks'],
            ['title' => '25', 'category_type'=>'mans-socks'],
            ['title' => '27', 'category_type'=>'mans-socks'],
            ['title' => '29', 'category_type'=>'mans-socks'],
            ['title' => '31', 'category_type'=>'mans-socks'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '42', 'category_type'=>'woman-clothes'],
            ['title' => '44', 'category_type'=>'woman-clothes'],
            ['title' => '46', 'category_type'=>'woman-clothes'],
            ['title' => '48', 'category_type'=>'woman-clothes'],
            ['title' => '50', 'category_type'=>'woman-clothes'],
            ['title' => '52', 'category_type'=>'woman-clothes'],
            ['title' => '54', 'category_type'=>'woman-clothes'],
            ['title' => 'XS', 'category_type'=>'woman-clothes'],
            ['title' => 'S', 'category_type'=>'woman-clothes'],
            ['title' => 'M', 'category_type'=>'woman-clothes'],
            ['title' => 'L', 'category_type'=>'woman-clothes'],
            ['title' => 'XL', 'category_type'=>'woman-clothes'],
        ]);

        $sizes->propertyValues()->createMany([
            ['title' => '42', 'category_type'=>'woman-jeans'],
            ['title' => '44', 'category_type'=>'woman-jeans'],
            ['title' => '46', 'category_type'=>'woman-jeans'],
            ['title' => '48', 'category_type'=>'woman-jeans'],
            ['title' => '50', 'category_type'=>'woman-jeans'],
            ['title' => '52', 'category_type'=>'woman-jeans'],
            ['title' => '54', 'category_type'=>'woman-jeans'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '40', 'category_type'=>'woman-dresses'],
            ['title' => '42', 'category_type'=>'woman-dresses'],
            ['title' => '44', 'category_type'=>'woman-dresses'],
            ['title' => '46', 'category_type'=>'woman-dresses'],
            ['title' => '48', 'category_type'=>'woman-dresses'],
            ['title' => '50', 'category_type'=>'woman-dresses'],
            ['title' => '52', 'category_type'=>'woman-dresses'],
            ['title' => '54', 'category_type'=>'woman-dresses'],
            ['title' => '56', 'category_type'=>'woman-dresses'],
            ['title' => '58', 'category_type'=>'woman-dresses'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '40', 'category_type'=>'woman-blouse'],
            ['title' => '42', 'category_type'=>'woman-blouse'],
            ['title' => '44', 'category_type'=>'woman-blouse'],
            ['title' => '46', 'category_type'=>'woman-blouse'],
            ['title' => '48', 'category_type'=>'woman-blouse'],
            ['title' => '50', 'category_type'=>'woman-blouse'],
        ]);

        $sizes->propertyValues()->createMany([
            ['title' => '42', 'category_type'=>'woman-underwear-panties'],
            ['title' => '44', 'category_type'=>'woman-underwear-panties'],
            ['title' => '46', 'category_type'=>'woman-underwear-panties'],
            ['title' => '48', 'category_type'=>'woman-underwear-panties'],
            ['title' => '50', 'category_type'=>'woman-underwear-panties'],
            ['title' => '52', 'category_type'=>'woman-underwear-panties'],
            ['title' => '54', 'category_type'=>'woman-underwear-panties'],
            ['title' => '56', 'category_type'=>'woman-underwear-panties'],
            ['title' => 'XXS', 'category_type'=>'woman-underwear-panties'],
            ['title' => 'XS', 'category_type'=>'woman-underwear-panties'],
            ['title' => 'S', 'category_type'=>'woman-underwear-panties'],
            ['title' => 'M', 'category_type'=>'woman-underwear-panties'],
            ['title' => 'L', 'category_type'=>'woman-underwear-panties'],
            ['title' => 'XL', 'category_type'=>'woman-underwear-panties'],
            ['title' => 'XXL', 'category_type'=>'woman-underwear-panties'],
            ['title' => 'XXXL', 'category_type'=>'woman-underwear-panties'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '65AA', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '65A', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '65B', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '70AA', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '70A', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '70B', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '70C', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '75A', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '75B', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '75C', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '75D', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '75E', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '80A', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '80B', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '80C', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '80D', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '80E', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '85A', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '85B', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '85C', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '85D', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '85E', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '90A', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '90B', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '90C', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '90D', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '90E', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '95A', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '95B', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '95C', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '95D', 'category_type'=>'woman-underwear-brassiere'],
            ['title' => '95E', 'category_type'=>'woman-underwear-brassiere'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '35', 'category_type'=>'woman-stockings'],
            ['title' => '36', 'category_type'=>'woman-stockings'],
            ['title' => '37', 'category_type'=>'woman-stockings'],
            ['title' => '38', 'category_type'=>'woman-stockings'],
            ['title' => '39', 'category_type'=>'woman-stockings'],
            ['title' => '40', 'category_type'=>'woman-stockings'],
            ['title' => '41', 'category_type'=>'woman-stockings'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '23', 'category_type'=>'woman-socks'],
            ['title' => '25', 'category_type'=>'woman-socks'],
            ['title' => '27', 'category_type'=>'woman-socks'],
            ['title' => '29', 'category_type'=>'woman-socks'],
            ['title' => '31', 'category_type'=>'woman-socks'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '34', 'category_type'=>'woman-shoes'],
            ['title' => '34,5', 'category_type'=>'woman-shoes'],
            ['title' => '35', 'category_type'=>'woman-shoes'],
            ['title' => '36', 'category_type'=>'woman-shoes'],
            ['title' => '36,5', 'category_type'=>'woman-shoes'],
            ['title' => '37', 'category_type'=>'woman-shoes'],
            ['title' => '37,5', 'category_type'=>'woman-shoes'],
            ['title' => '38', 'category_type'=>'woman-shoes'],
            ['title' => '39', 'category_type'=>'woman-shoes'],
            ['title' => '39,5', 'category_type'=>'woman-shoes'],
            ['title' => '40', 'category_type'=>'woman-shoes'],
            ['title' => '41', 'category_type'=>'woman-shoes'],
            ['title' => '41,5', 'category_type'=>'woman-shoes'],
            ['title' => '42', 'category_type'=>'woman-shoes'],
            ['title' => '42,5', 'category_type'=>'woman-shoes'],
        ]);

        $sizes->propertyValues()->createMany([
            ['title' => '39', 'category_type'=>'mans-shoes'],
            ['title' => '39,5', 'category_type'=>'mans-shoes'],
            ['title' => '40', 'category_type'=>'mans-shoes'],
            ['title' => '40,5', 'category_type'=>'mans-shoes'],
            ['title' => '41', 'category_type'=>'mans-shoes'],
            ['title' => '41,5', 'category_type'=>'mans-shoes'],
            ['title' => '42', 'category_type'=>'mans-shoes'],
            ['title' => '42,5', 'category_type'=>'mans-shoes'],
            ['title' => '43', 'category_type'=>'mans-shoes'],
            ['title' => '43,5', 'category_type'=>'mans-shoes'],
            ['title' => '44', 'category_type'=>'mans-shoes'],
            ['title' => '45', 'category_type'=>'mans-shoes'],
            ['title' => '46', 'category_type'=>'mans-shoes'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '6', 'category_type'=>'gloves'],
            ['title' => '6,5', 'category_type'=>'gloves'],
            ['title' => '7', 'category_type'=>'gloves'],
            ['title' => '7,5', 'category_type'=>'gloves'],
            ['title' => '8', 'category_type'=>'gloves'],
            ['title' => '8,5', 'category_type'=>'gloves'],
            ['title' => '9', 'category_type'=>'gloves'],
            ['title' => '9,5', 'category_type'=>'gloves'],
            ['title' => '10', 'category_type'=>'gloves'],
            ['title' => '10,5', 'category_type'=>'gloves'],
            ['title' => '11', 'category_type'=>'gloves'],
            ['title' => '11,5', 'category_type'=>'gloves'],
            ['title' => '12', 'category_type'=>'gloves'],
            ['title' => 'XXS', 'category_type'=>'gloves'],
            ['title' => 'XS', 'category_type'=>'gloves'],
            ['title' => 'S', 'category_type'=>'gloves'],
            ['title' => 'M', 'category_type'=>'gloves'],
            ['title' => 'L', 'category_type'=>'gloves'],
            ['title' => 'XL', 'category_type'=>'gloves'],
            ['title' => 'XXL', 'category_type'=>'gloves'],
            ['title' => 'XXXL', 'category_type'=>'gloves'],

        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '54', 'category_type'=>'hats'],
            ['title' => '55', 'category_type'=>'hats'],
            ['title' => '56', 'category_type'=>'hats'],
            ['title' => '57', 'category_type'=>'hats'],
            ['title' => '58', 'category_type'=>'hats'],
            ['title' => '59', 'category_type'=>'hats'],
            ['title' => '60', 'category_type'=>'hats'],
            ['title' => '61', 'category_type'=>'hats'],
            ['title' => '62', 'category_type'=>'hats'],
            ['title' => '63', 'category_type'=>'hats'],
            ['title' => '64', 'category_type'=>'hats'],
            ['title' => '65', 'category_type'=>'hats'],
            ['title' => 'XXS', 'category_type'=>'hats'],
            ['title' => 'XS', 'category_type'=>'hats'],
            ['title' => 'S', 'category_type'=>'hats'],
            ['title' => 'M', 'category_type'=>'hats'],
            ['title' => 'L', 'category_type'=>'hats'],
            ['title' => 'XL', 'category_type'=>'hats'],
            ['title' => 'XXL', 'category_type'=>'hats'],
            ['title' => 'XXL', 'category_type'=>'hats'],
            ['title' => 'XXXL', 'category_type'=>'hats'],
            ['title' => 'XXXL', 'category_type'=>'hats'],
            ['title' => 'XXXXL', 'category_type'=>'hats'],
            ['title' => 'XXXXL', 'category_type'=>'hats'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '15', 'category_type'=>'rings'],
            ['title' => '15,5', 'category_type'=>'rings'],
            ['title' => '15,75', 'category_type'=>'rings'],
            ['title' => '16', 'category_type'=>'rings'],
            ['title' => '16,5', 'category_type'=>'rings'],
            ['title' => '17', 'category_type'=>'rings'],
            ['title' => '17,25', 'category_type'=>'rings'],
            ['title' => '17,75', 'category_type'=>'rings'],
            ['title' => '18', 'category_type'=>'rings'],
            ['title' => '18,5', 'category_type'=>'rings'],
            ['title' => '19', 'category_type'=>'rings'],
            ['title' => '19,5', 'category_type'=>'rings'],
            ['title' => '20', 'category_type'=>'rings'],
            ['title' => '20,25', 'category_type'=>'rings'],
            ['title' => '25,75', 'category_type'=>'rings'],
            ['title' => '21', 'category_type'=>'rings'],
            ['title' => '21,25', 'category_type'=>'rings'],
            ['title' => '21,75', 'category_type'=>'rings'],
            ['title' => '22', 'category_type'=>'rings'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '50', 'category_type'=>'children_0_7'],
            ['title' => '56', 'category_type'=>'children_0_7'],
            ['title' => '62', 'category_type'=>'children_0_7'],
            ['title' => '68', 'category_type'=>'children_0_7'],
            ['title' => '74', 'category_type'=>'children_0_7'],
            ['title' => '80', 'category_type'=>'children_0_7'],
            ['title' => '86', 'category_type'=>'children_0_7'],
            ['title' => '92', 'category_type'=>'children_0_7'],
            ['title' => '98', 'category_type'=>'children_0_7'],
            ['title' => '104', 'category_type'=>'children_0_7'],
            ['title' => '110', 'category_type'=>'children_0_7'],
            ['title' => '116', 'category_type'=>'children_0_7'],
            ['title' => '122', 'category_type'=>'children_0_7'],
            ['title' => '128', 'category_type'=>'children_0_7'],
            ['title' => '134', 'category_type'=>'children_0_7'],
            ['title' => '140', 'category_type'=>'children_0_7'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '152', 'category_type'=>'children_boys_8_18'],
            ['title' => '158', 'category_type'=>'children_boys_8_18'],
            ['title' => '164', 'category_type'=>'children_boys_8_18'],
            ['title' => '170', 'category_type'=>'children_boys_8_18'],
            ['title' => '176', 'category_type'=>'children_boys_8_18'],
            ['title' => '182', 'category_type'=>'children_boys_8_18'],
            ['title' => '188', 'category_type'=>'children_boys_8_18'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '152', 'category_type'=>'children_girls_8_18'],
            ['title' => '158', 'category_type'=>'children_girls_8_18'],
            ['title' => '164', 'category_type'=>'children_girls_8_18'],
            ['title' => '170', 'category_type'=>'children_girls_8_18'],
            ['title' => '176', 'category_type'=>'children_girls_8_18'],
            ['title' => '182', 'category_type'=>'children_girls_8_18'],
            ['title' => '188', 'category_type'=>'children_girls_8_18'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => 'до 3', 'category_type'=>'children_babies'],
            ['title' => '0–3 месяцев', 'category_type'=>'children_babies'],
            ['title' => '3–6 месяцев', 'category_type'=>'children_babies'],
            ['title' => '6–9 месяцев', 'category_type'=>'children_babies'],
            ['title' => '9–12 месяцев', 'category_type'=>'children_babies'],
            ['title' => '12–18 месяцев', 'category_type'=>'children_babies'],
            ['title' => '18–24 месяцев', 'category_type'=>'children_babies'],
            ['title' => '2T', 'category_type'=>'children_babies'],
            ['title' => '3Т', 'category_type'=>'children_babies'],
            ['title' => '4Т', 'category_type'=>'children_babies'],
            ['title' => '5Т', 'category_type'=>'children_babies'],
        ]);
        $sizes->propertyValues()->createMany([
            ['title' => '4', 'age'=>'3-4', 'category_type'=>'children_clothes'],
            ['title' => '5', 'age'=>'4-5', 'category_type'=>'children_clothes'],
            ['title' => '6', 'age'=>'5-6', 'category_type'=>'children_clothes'],
            ['title' => '6X/7', 'age'=>'6-7', 'category_type'=>'children_clothes'],
            ['title' => '8', 'age'=>'7-8', 'category_type'=>'children_clothes'],
            ['title' => '10', 'age'=>'8-9', 'category_type'=>'children_clothes'],
            ['title' => '12', 'age'=>'9-10', 'category_type'=>'children_clothes'],
            ['title' => '14', 'age'=>'10-12', 'category_type'=>'children_clothes'],
            ['title' => '16', 'age'=>'12', 'category_type'=>'children_clothes'],
            ['title' => 'XS', 'age'=>'3-4', 'category_type'=>'children_clothes'],
            ['title' => 'S', 'age'=>'4-5', 'category_type'=>'children_clothes'],
            ['title' => 'S', 'age'=>'5-6', 'category_type'=>'children_clothes'],
            ['title' => 'M', 'age'=>'6-7', 'category_type'=>'children_clothes'],
            ['title' => 'M', 'age'=>'7-8', 'category_type'=>'children_clothes'],
            ['title' => 'L', 'age'=>'8-9', 'category_type'=>'children_clothes'],
            ['title' => 'L', 'age'=>'9-10', 'category_type'=>'children_clothes'],
            ['title' => 'XL', 'age'=>'10-12', 'category_type'=>'children_clothes'],
            ['title' => 'XL', 'age'=>'12', 'category_type'=>'children_clothes'],
        ]);
    }
}
