<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Region;
use Illuminate\Http\Request;
use Vanilo\Properties\Models\Property;
use Vanilo\Properties\Models\PropertyValue;
use Illuminate\Support\Facades\Auth;
use App\Models\Tender;

class TendersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tenders = Tender::getTenders();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = Region::where('depth', '<=', 3)->orderBy('id')->get();
        $categories = Category::where('depth', '>=', 1)->orderBy('id')->get();
        $sizes = Property::findBySlug('size');
        $sizes = PropertyValue::byProperty($sizes)->get();
        return view('lk.tenders.create', [
                'categories'=>$categories,
                'regions'=>$regions,
                'sizes'  =>$sizes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach($request->category as $category){
            $categories[]['category_id'] = $category;
        }

        foreach($request->segment as $segment){
            $segments[]['name'] = $segment;
        }
        $data = [
            'user_id'=>Auth::id(),
            'categories'=>json_encode($categories),
            'sum'=>(double)$request->min_order,
            'volume'=>json_encode($request->opt),
            'segment'=>json_encode($request->segment),
            'region_delivery'=>$request->delivery_cover,
            'parameters' => json_encode(['sizes'=>$request->sizes]),
        ];

        Tender::create($data);

        //return redirect()->route('platform.tenders');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
