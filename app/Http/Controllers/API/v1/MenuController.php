<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Menu\MenuServiceProvider;
use Konekt\Menu\Facades\Menu;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = [];

        foreach(Menu::get('left_nav_menu')->items->roots() as $item)
        {
            if ($item->hasLink() && $item->isAllowed()){
                $menu[$item->name]['hasLink'] = true;
                $menu[$item->name]['attr']['class'] = $item->attr('class') ?? '';
                $menu[$item->name]['link']['attr']['class'] = $item->link->attr('class');
                $withoutProtocolDelimiter = str_replace('//', '', $item->url());
                $menu[$item->name]['url'] = substr($withoutProtocolDelimiter, strpos($withoutProtocolDelimiter, '/'));
                if($item->data('right_text')){
                    $menu[$item->name]['data']['right_text'] = $item->data('right_text');
                    $menu[$item->name]['data']['class_text'] = $item->data('class_text');
                }
                if($item->data('icon')){
                    $menu[$item->name]['data']['icon'] = $item->data('icon');
                }
                $menu[$item->name]['title'] = $item->title;
            }
            else {
                if ($item->hasChildren()) {
                    if ($item->childrenAllowed()->count()) {
                        $menu[$item->name]['hasChildren'] = true;
                        $menu[$item->name]['hasActiveChild'] = $item->hasActiveChild();
                        if ($item->data('icon')) {
                            $menu[$item->name]['data']['icon'] = $item->data('icon');
                        }
                        if ($item->data('right_text')) {
                            $menu[$item->name]['data']['right_text'] = $item->data('right_text');
                            $menu[$item->name]['data']['class_text'] = $item->data('class_text');
                        }
                        if ($item->data('arrow')) {
                            $menu[$item->name]['data']['arrow'] = $item->data('arrow');
                        }
                        $menu[$item->name]['title'] = $item->title;
                        $menu[$item->name]['name'] = $item->name;
                        $menu[$item->name]['url'] = 'sidebar-submenu-'.$item->name;

                        foreach ($item->children() as $childItem) {
                            if ($childItem->isAllowed()) {
                                $menu[$item->name]['children'][$childItem->name]['attr']['class'] = $childItem->attr('class');
                                $withoutProtocolDelimiter = str_replace('//', '', $childItem->url());
                                $menu[$item->name]['children'][$childItem->name]['url'] = substr($withoutProtocolDelimiter, strpos($withoutProtocolDelimiter, '/'));
                                if ($childItem->data('icon')) {
                                    $menu[$item->name]['children'][$childItem->name]['data']['icon'] = $childItem->data('icon');
                                }
                                $menu[$item->name]['children'][$childItem->name]['title'] = $childItem->title;
                            }

                            if ($childItem->hasChildren()) {
                                if ($childItem->childrenAllowed()->count()) {
                                    $menu[$item->name]['children'][$childItem->name]['hasChildren'] = true;
                                    $menu[$item->name]['children'][$childItem->name]['hasActiveChild'] = $childItem->hasActiveChild();
                                    if ($childItem->data('icon')) {
                                        $menu[$item->name][$childItem->name]['data']['icon'] = $childItem->data('icon');
                                    }
                                    if ($item->data('right_text')) {
                                        $menu[$item->name]['children'][$childItem->name]['data']['right_text'] = $childItem->data('right_text');
                                        $menu[$item->name]['children'][$childItem->name]['data']['class_text'] = $childItem->data('class_text');
                                    }
                                    if ($item->data('arrow')) {
                                        $menu[$item->name]['children'][$childItem->name]['data']['arrow'] = $childItem->data('arrow');
                                    }
                                    $menu[$item->name]['children'][$childItem->name]['title'] = $childItem->title;
                                    $menu[$item->name]['children'][$childItem->name]['name'] = $childItem->name;

                                    foreach ($childItem->children() as $sub_childItem) {
                                        if ($sub_childItem->isAllowed()) {
                                            $menu[$item->name]['children'][$childItem->name]['children'][$sub_childItem->name]['attr']['class'] = $sub_childItem->attr('class');
                                            $menu[$item->name]['children'][$childItem->name]['children'][$sub_childItem->name]['url'] = $sub_childItem->url();
                                            if ($sub_childItem->data('icon')) {
                                                $menu[$item->name]['children'][$childItem->name]['data'][$sub_childItem->name]['icon'] = $sub_childItem->data('icon');
                                            }
                                            $menu[$item->name]['children'][$childItem->name]['children'][$sub_childItem->name]['title'] = $sub_childItem->title;
                                            $withoutProtocolDelimiter = str_replace('//', '', $sub_childItem->url());
                                            $menu[$item->name]['url'] = substr($withoutProtocolDelimiter, strpos($withoutProtocolDelimiter, '/'));
                                        }
                                    }
                                }
                            } elseif ($item->isAllowed()) {
                                $menu[$item->name]['attr']['class'] = $item->attr('class');
                                if ($item->data('icon')) {
                                    $menu[$item->name]['data']['icon'] = $item->data('icon');
                                }
                                if ($item->data('right_text')) {
                                    $menu[$item->name]['data']['right_text'] = $item->data('right_text');
                                    $menu[$item->name]['data']['class_text'] = $item->data('class_text');
                                }
                                $menu[$item->name]['title'] = $item->title;
                            }



                        }
                    }
                } elseif ($item->isAllowed()) {
                    $menu[$item->name]['hasLink'] = false;
                    $menu[$item->name]['attr']['class'] = $item->attr('class');
                    if ($item->data('icon')) {
                        $menu[$item->name]['data']['icon'] = $item->data('icon');
                    }
                    if ($item->data('right_text')) {
                        $menu[$item->name]['data']['right_text'] = $item->data('right_text');
                        $menu[$item->name]['data']['class_text'] = $item->data('class_text');
                    }
                    $menu[$item->name]['title'] = $item->title;
                }
            }
            //$menu[] = $item;
        }
        /*
         *  @foreach($leftNavMenu->items->roots() as $item)
                    @if ($item->hasLink() && $item->isAllowed())
                        <li class="{{ $item->attr('class') }}">
                            <a class="{{ $item->link->attr('class') }}" href="{!! $item->url() !!}">
                                @if($item->data('right_text'))
                                    <span class="{{$item->data('class_text')}}">{{$item->data('right_text')}}</span>
                                @endif
                                @if($item->data('icon'))
                                    <i data-feather="{!! $item->data('icon') !!}"></i>
                                @endif
                                <span>{!! $item->title !!}</span>
                            </a>
                        </li>
                    @else
                        @if($item->hasChildren())
                            @if($item->childrenAllowed()->count())
                                <li class="nav-item nav-dropdown{{ $item->hasActiveChild() ? ' open' : '' }}">
                                    <a href="#sidebar-submenu-{{$item->name}}" class="nav-link nav-dropdown-toggle" data-toggle="collapse" aria-expanded="{{ $item->hasActiveChild() ? 'true' : 'false' }}">
                                        @if($item->data('icon'))
                                            <i data-feather="{!! $item->data('icon') !!}"></i>
                                        @endif
                                        @if($item->data('right_text'))
                                            <span class="{{$item->data('class_text')}}">{{$item->data('right_text')}}</span>
                                        @endif
                                        <span>{!! $item->title !!}</span>
                                        @if($item->data('arrow'))
                                                <span class="{!! $item->data('arrow') !!}"></span>
                                        @endif
                                    </a>
                                    <div class="collapse{{ $item->hasActiveChild() ? ' show' : '' }}" id="sidebar-submenu-{{$item->name}}">
                                        <ul class="nav-second-level">
                                            @foreach($item->children() as $childItem)
                                                @if($childItem->isAllowed())
                                                    <li class="{{ $childItem->attr('class') }}">
                                                        <a class="{{ $childItem->link->attr('class') }}" href="{!! $childItem->url() !!}">
                                                            @if($childItem->data('icon'))
                                                                <i data-feather="{!! $childItem->data('icon') !!}"></i>
                                                            @endif
                                                            {!! $childItem->title !!}
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                            @endif
                        @elseif ($item->isAllowed())
                            <li class="menu-title {{ $item->attr('class') }}">
                                @if($item->data('icon'))
                                    <i data-feather="{!! $item->data('icon') !!}"></i>
                                @endif
                                @if($item->data('right_text'))
                                    <span class="{{$item->data('class_text')}}">{{$item->data('right_text')}}</span>
                                @endif
                                {!! $item->title !!}
                            </li>
                        @endif
                    @endif
                @endforeach
         */

        //dd(json_encode($menu));
        return response()->json($menu, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
