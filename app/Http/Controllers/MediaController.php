<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Vanilo\Framework\Http\Controllers\MediaController as VaniloMediaController;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Vanilo\Framework\Contracts\Requests\CreateMedia;

class MediaController extends VaniloMediaController
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMedia $request)
    {
        $key = csrf_token();

        session(["img_".$key => $request]);

        dd(session("img_".$key));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Media $medium)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
