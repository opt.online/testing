<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPHtmlParser\Dom;

class ParserController extends Controller
{
    public function index(){

        $start = 5;
        $end = 15;
        $max = 720;
        $dom = new Dom;
        for($i = $start; $i <= $end; $i++){
            $dom->loadFromUrl('https://optlist.ru/manufacturers?sort=new&limit=100&page='.$i);
            $contents = $dom->find('a[itemprop="url"]');
            echo 'https://optlist.ru/manufacturers?page='.$i."<br/>";

            foreach ($contents as $content)
            {
                // get the class attr
                $href = $content->getAttribute('href');
                //echo $dom->loadFromUrl('https://optlist.ru'.$href);

                $href = "https://optlist.ru".$href;

                // Create a stream
                $opts = [
                    "http" => [
                        "method" => "GET",
                        "header" => ":authority: optlist.ru\r\n".
                            ":method: GET\r\n".
                            ":path: /company/ol-laboratory\r\n".
                            ":scheme: https\r\n".
                            "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\r\n".
                            "accept-encoding: gzip, deflate, br\r\n".
                            "accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7\r\n".
                            "cache-control: max-age=0\r\n".
                            "cookie: _ga=GA1.2.647359567.1597846839; _ym_uid=1597846839624149805; __gads=ID=c4d6b6bbf3994a1c-223d4cb6fab80074:T=1602681734:RT=1602681734:S=ALNI_MZXXCuXYLkU_MlkeKf7Z8wUXWVzaA; cookie_policy=1; _ym_d=1613633382; _gid=GA1.2.2059807542.1618903148; _ym_visorc=w; _ym_isad=2; s=MTYxODkwNjgwOHxEdi1CQkFFQ180SUFBUkFCRUFBQV83Yl9nZ0FFQm5OMGNtbHVad3dHQUFSdFFXZGxCV2x1ZERZMEJBWUFfT2FiaGZnR2MzUnlhVzVuREFjQUJXeHZaMmx1Qm5OMGNtbHVad3diQUJsemRHRnllVzl6YTI5c2NHRnphR0ZBZVdGdVpHVjRMbkoxQm5OMGNtbHVad3dGQUFOaFkyTUdjM1J5YVc1bkRDWUFKRGcyWXpGaFpETXhMVFZtT1RNdE5HWTVOeTA1WXpSaExUTTNNVEF4TmpZd05tWXdOZ1p6ZEhKcGJtY01CZ0FFYm1GdFpRWnpkSEpwYm1jTURBQUswSl9Rc05DeTBMWFF1dz09fA8AtL1eUDzNz0if2-qVg32LhINwrLz6nUjSOVy7drhC\r\n".
                            "referer: https://optlist.ru/\r\n".
                            "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36\r\n"
                    ]
                ];

// DOCS: https://www.php.net/manual/en/function.stream-context-create.php
                $context = stream_context_create($opts);

// Open the file using the HTTP headers set above
// DOCS: https://www.php.net/manual/en/function.file-get-contents.php
                $file = file_get_contents($href, false, $context);

                $dom->loadStr($file);
                $email = $dom->find('span[itemprop="email"]');
                $site = $dom->find('a[itemprop="url"]');

                if(sizeOf($email) > 0)
                    $email = $email->text;
                if(sizeOf($site) > 0)
                    $site = $site->getAttribute('href');
                file_put_contents('manufactures.csv', $href.";".$email.";".$site.";".$i."\n", FILE_APPEND | LOCK_EX);
                //echo $site; // "click here"

                //echo $file;

                //$phone = $dom->find('a');
                //echo $phone;

                // do something with the html
                //$html = $content->innerHtml;

                // or refine the find some more
                //$child   = $content->firstChild();
                //$sibling = $child->nextSibling();

                sleep(1);
            }
        }
    }



    public function postavschiki()
    {

        $href = 'https://www.postavshhiki.ru/scripts/show_contacts.php';

        // Create a stream
        $opts = [
                    "http" => [
                        "method" => "POST",
                        "header" => ":authority: www.postavshhiki.ru\r\n" .
                        ":method: POST\r\n" .
                        ":path: /scripts/show_contacts.php\r\n" .
                        ":scheme: https\r\n" .
                        "accept: */*\r\n" .
                        "accept-encoding: gzip, deflate, br\r\n" .
                        "accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7\r\n" .
                        "content-length: 15\r\n" .
                        "content-type: application/x-www-form-urlencoded\r\n" .
                        "cookie: _ga=GA1.2.1612046292.1597846542; _ym_uid=1597846542584608738; top100_id=t1.-1.243150391.1597846548314; tmr_lvid=c8523235a1d5a959585c09d9b7ff6b6b; tmr_lvidTS=1597846548376; __gads=ID=db59973dd02ffb25-222b54baf8b8006d:T=1602691072:RT=1602691072:S=ALNI_MbPi5rtEQIBCkC2969zYVH0aNo4Iw; _ym_d=1614856803; _gid=GA1.2.193635905.1618919075; _ym_isad=2; _ym_visorc=w; c80708056c79a484e3f521b1e1310bdc=pmj4f6eg1dejrth6p1msv7tjr7; joomla_user_state=logged_in; swp_token=1618922489:3ed126b4674b178b213ee85707f42c40:ccd21e835e2176699f0882d865ecb663; _gat=1; last_visit=1618910133890::1618920933890; tmr_detect=0%7C1618920937529; tmr_reqNum=1351\r\n" .
                        "origin: https://www.postavshhiki.ru\r\n" .
                        "referer: https://www.postavshhiki.ru/optovye-pokupateli/40-tovary-dlya-sporta-i-otdykha/12379-ooo-mlada\r\n" .
                        "sec-ch-ua: \"Google Chrome\";v=\"89\", \"Chromium\";v=\"89\", \";Not A Brand\";v=\"99\"\r\n" .
                        "sec-ch-ua-mobile: ?0\r\n" .
                        "sec-fetch-dest: empty\r\n" .
                        "sec-fetch-mode: cors\r\n" .
                        "sec-fetch-site: same-origin\r\n" .
                        "user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36\r\n" .
                        "x-requested-with: XMLHttpRequest\r\n",
                        "content" => "id:12379\r\n" .
                                     "type:4\r\n"
                    ]
                ];

// DOCS: https://www.php.net/manual/en/function.stream-context-create.php
                $context = stream_context_create($opts);

// Open the file using the HTTP headers set above
// DOCS: https://www.php.net/manual/en/function.file-get-contents.php
                $file = file_get_contents($href, false, $context);

                //echo $site; // "click here"

                echo $file;

                //$phone = $dom->find('a');
                //echo $phone;

                // do something with the html
                //$html = $content->innerHtml;

                // or refine the find some more
                //$child   = $content->firstChild();
                //$sibling = $child->nextSibling();

                sleep(1);
    }
}
