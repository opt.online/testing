<?php


namespace App\Services;


use App\Models\Organization;
use Illuminate\Support\Facades\Auth;

class CompanyService
{

    public function __construct()
    {

    }

    public static function getUserCompany(){
        $organization = Organization::where('user_id', Auth::id())->first();
        if(empty($organization)){
            return null;
        }
        return $organization;
    }

}
