<?php

namespace App\Services;

use Vanilo\Order\Contracts\Order;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Vanilo\Checkout\Contracts\Checkout;
use Vanilo\Contracts\CheckoutSubject;
use Vanilo\Framework\Factories\OrderFactory;
use Vanilo\Order\Contracts\OrderNumberGenerator;
use Vanilo\Order\Events\BaseOrderEvent;

class OrderService extends OrderFactory
{


    public function __construct(OrderNumberGenerator $generator)
    {
        $this->orderNumberGenerator = $generator;
    }

    public function createFromCheckout(Checkout $checkout)
    {
        $orderData = [
            'billpayer'       => $checkout->getBillpayer()->toArray(),
            'shippingAddress' => $checkout->getShippingAddress()->toArray()
        ];

        $items = $this->convertCartItemsToDataArray($checkout->getCart());

        return $this->createFromDataArray($orderData, $items);
    }

    protected function convertCartItemsToDataArray(CheckoutSubject $cart)
    {
        return $cart->getItems()->map(function ($item) {
            return [
                'product'  => $item->getBuyable(),
                'quantity' => $item->getQuantity()
            ];
        })->all();
    }


    public function createFromDataArray(array $data, array $items): \Vanilo\Order\Contracts\Order
    {
        if (empty($items)) {
            throw new CreateOrderException(__('Can not create an order without items'));
        }

        DB::beginTransaction();

        try {

            dd($items);
            $order = app(Order::class);

            $order->fill(Arr::except($data, ['billpayer', 'shippingAddress']));
            $order->number  = $data['number'] ?? $this->orderNumberGenerator->generateNumber($order);
            $order->user_id = $data['user_id'] ?? auth()->id();

            dd($order);
            $order->save();

            $this->createBillpayer($order, $data);
            $this->createShippingAddress($order, $data);

            $this->createItems(
                $order,
                array_map(function ($item) {
                    // Default quantity is 1 if unspecified
                    $item['quantity'] = $item['quantity'] ?? 1;

                    return $item;
                }, $items)
            );

            $order->save();
        } catch (\Exception $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        event(new OrderWasCreated($order));

        return $order;
    }

}
