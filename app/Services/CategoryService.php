<?php

namespace App\Services;

use App\Models\Category;
use App\Models\ProductModel;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

class CategoryService
{

    public function __construct()
    {

    }

    public static function getCategoryByName($categoryName){
        $category = Category::where('name', $categoryName)->first();
        if(empty($category)){
            return null;
        }
        return $category;
    }
}