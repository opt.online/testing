<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;

class Tender extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'categories',
        'description',
        'sum',
        'count',
        'volume',
        'segment',
        'region_delivery',
        'parameters',
        'status',
        'date_to'
    ];

    protected $casts = [
        'categories' => 'array',
        'segment' => 'json',
    ];

    public static function getTenders(){

        $tenders = Tender::where('user_id', Auth::id())->get();

        return $tenders;


            $category_id = 1;
            $result = Category::defaultOrder()->descendantsAndSelf($category_id);
            dd($result);
            foreach ($result as $r){
                $tender = Tender::whereRaw('("categories")::jsonb @> ANY (array [\'[{"category_id":"'.$r['id'].'"}]\']::jsonb[])')->paginate();
                if($tender->count() > 0){
                    foreach ($tender as $t){
                        $tenders[$t->id] = $t;
                    }
                }
                //unset($tender);
            }

        $data = [];
        $i=0;
        foreach ($tenders as $tender){
                $data[$i]['tender_id'] = $tender['id'];
                //dd($tender->items()[0]['user_id']);
                $user = User::find($tender['user_id']);
                //dd($user);
                $data[$i]['user_name'] = $user->name;
                $data[$i]['description'] = $tender['description'];
                foreach ($tender['categories'] as $key=>$value){
                    $category = Category::find($value['category_id']);
                    $data[$i]['categories'][$key] = $category->name;
                }
                $data[$i]['sum'] = $tender['sum'];
                $data[$i]['count'] = $tender['count'];
                $data[$i]['volume'] = ($tender['volume'] == 'small') ? 'Мелкий опт' : ($tender['volume'] == 'middle' ? 'Средний опт' : 'Крупный опт');
                foreach ($tender['segment'] as $key=>$value){
                    $data[$i]['segment'][$key] = ($value['name'] == 'middle') ? 'Средний сегмент' : ($value['name'] == 'high' ? 'Высокий сегмент' : 'Премиум сегмент');
                }
                $region = Region::select('name')->where('id', $tender['region_delivery'])->first();
                $data[$i]['region_delivery'] = $region->name;
                $data[$i]['date'] = $tender->created_at;
            $i++;
        }

        return $data;
    }

    public static function getTender($id){
        $tender = Tender::findOrFail($id);
        $data['tender_id'] = $tender['id'];

        $user = User::find($tender['user_id']);
        //dd($user);
        $data['user_name'] = $user->name;
        $data['description'] = $tender['description'];
        foreach ($tender['categories'] as $key=>$value){
            $category = Category::find($value['category_id']);
            $data['categories'][$key] = $category->name;
        }
        $data['sum'] = $tender['sum'];
        $data['count'] = $tender['count'];
        $data['volume'] = ($tender['volume'] == 'small') ? 'Мелкий опт' : ($tender['volume'] == 'middle' ? 'Средний опт' : 'Крупный опт');
        foreach ($tender['segment'] as $key=>$value){
            $data['segment'][$key] = ($value['name'] == 'middle') ? 'Средний сегмент' : ($value['name'] == 'high' ? 'Высокий сегмент' : 'Премиум сегмент');
        }
        $region = Region::select('name')->where('id', $tender['region_delivery'])->first();
        $data['region_delivery'] = $region->name;
        $data['date'] = $tender->created_at;

        return $data;
    }

    public static function getTendersCategory(){
        $category_id = 15;
        $result = Category::defaultOrder()->descendantsAndSelf($category_id);

        return $result;
    }

    public static function getTendersByCategoryUserId($user_id){
        $tenders = Tender::select('categories')->where('user_id', $user_id)->get();
        return $tenders;
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function users()
    {
        return $this->hasOne(User::class);
    }
}
