<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Vanilo\Framework\Models\Product as VaniloProduct;
use Vanilo\Product\Contracts\Product as ProductContract;
use Vanilo\Properties\Models\Property;
use App\Services\ProductService;

class Product extends VaniloProduct implements ProductContract
{

    use HasApiTokens;

    protected $enums = [
        'status'=>ProductsEnum::class,
    ];

    public function isActive(): bool
    {
        return $this->status->isActive();
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function properties($product_id)
    {
        return ProductService::getProperties($product_id);
    }
}
