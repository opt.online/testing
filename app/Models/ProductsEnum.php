<?php

namespace App\Models;

use Vanilo\Product\Models\ProductState;

class ProductsEnum extends ProductState
{
    const NEW_STATE         = 'new';
    const DRAFT_STATE       = 'draft';
    const INACTIVE_STATE    = 'inactive';
    const ACTIVE_STATE      = 'active';
    const UNAVAILABLE_STATE = 'unavailable';
    const PROMO_STATE       = 'promo';
    const HIDDEN_STATE      = 'hidden';
    const DELETED_STATE     = 'deleted';
    const RECOMMENDED_STATE = 'recommended';
    const REPEAT_STATE      = 'repeat';
    const PREORDER_STATE    = 'preorder';
    
    const __DEFAULT = self::HIDDEN_STATE;

    protected static $activeStates = [
        self::NEW_STATE,
        self::ACTIVE_STATE,
        self::PROMO_STATE,
        self::RECOMMENDED_STATE,
        self::REPEAT_STATE,
        self::PREORDER_STATE
    ];
}