<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Vanilo\Cart\Models\Cart as VaniloCart;

class Cart extends VaniloCart
{
    use HasFactory;

    protected $fillable = [];
}
