<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelPropertyValue extends Model
{
    use HasFactory;

    protected $fillable = [
        'property_value_id',
        'model_type',
        'model_id',
        'stock'
    ];
}
