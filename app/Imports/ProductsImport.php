<?php

namespace App\Imports;

use App\Models\Organization;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        dd($row);
       /* if($row['product-modification'] != 0){
            //$product_id = $row['product-modification'];
            $is_modification = true;
        }
        else{*/
            $product_id = Str::uuid();
            $is_modification = false;
        //}


        $company_id = Organization::select('id')->where('user_id', Auth::id())->first();

        $product_data = [
            "product_id" => $product_id->id,
            "company_id" => $company_id,
            "name" => $row["name"] ?? '',
            "barcode" => $row["barcode"] ?? 0000000,
            "sku" => $row["sku"] ?? '',
            "user_id" => Auth::id(),
            "model" => $row["model"],
            "country_made_in" => $row["country_made_in"] ?? 0,
            "description" => $row["description"],
            "category_id" => $row["category_id"] ?? 0,
            "category_group_id" => $row["category_group_id"] ?? 0,
            "price" => $row["price"] ?? 0,
            //"dropshipping_price" => $row["dropshipping_price"],
            "discount" => $row["discount"] ?? 0,
            "recommended_retail_price" => $row["recommended_retail_price"] ?? 0,
            "min_order" => $row["min_order"] ?? 1,
            "status" => $row["status"] ?? 'hidden',
            "segment" => $row["segment"] ?? '',
            //"stock" => array_sum($row['stock'] ?? []),
            'is_modification' => $is_modification,
        ];

        dd($product_data);

        $product = ProductService::saveProduct($product_data);

        return $product;

        /*return new Product([

        ]);*/
    }

}
