<?php

namespace App\Providers;

use App\Models\UserEnum;
use Illuminate\Support\ServiceProvider;
use Konekt\Address\Models\Organization;
use Konekt\User\Contracts\UserType;
use Vanilo\Product\Contracts\Product as ProductContract;
use Vanilo\Product\Contracts\ProductState;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
           $this->app->concord->registerModel(\Konekt\User\Contracts\User::class, \App\Models\User::class);
           $this->app->concord->registerModel(ProductContract::class, \App\Models\Product::class);
           $this->app->concord->registerModel(ProductState::class, \App\Models\ProductsEnum::class);
           $this->app->concord->registerModel(UserType::class, UserEnum::class);
           $this->app->concord->registerModel(Organization::class, \App\Models\Organization::class);
    }
}
