<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['web','auth'], 'prefix' => 'lk', 'as'=>'lk.'], function () {
    /*Route::get('products/upload', 'ProductsController@uploadFile')->name('products.upload');
    Route::resource('products', 'ProductsController');
    Route::resource('media', 'MediaController')->only(['update', 'destroy', 'store']);
    Route::resource('cart', 'CartController');
    Route::resource('checkout', 'CheckoutController');
    Route::resource('orders', 'OrderController');
    Route::resource('dashboard', 'DashboardController');
    //Route::resource('company', 'CompanyController');
    Route::resource('tenders', 'TendersController');
    Route::get('/requests/new', 'RequestsController@getNewRequest')->name('requests.new');
    Route::resource('requests', 'RequestsController');


    Route::post('/distributor/add_link', 'DistributorController@addLink')->name('distributor.add_link');*/

    Route::get('{any}', 'RoutingController@lk')->where('any', '.*');
});

Route::group(['middleware' => 'web', 'prefix' => '/'], function () {
    //Route::get('/login', 'RoutingController@auth')->name('login');
    //Route::get('/register', 'RoutingController@auth')->name('register');
    //Route::post('/register', 'Auth\RegisterController@register');
    Route::get('{first}/{second}/{third}', 'RoutingController@thirdLevel')->name('third');
    Route::get('{first}/{second}', 'RoutingController@secondLevel')->name('second');
    Route::get('{any}', 'RoutingController@root')->name('any');
});
// landing
Route::get('', 'RoutingController@index')->name('index');



Route::get('/login', 'RoutingController@auth');

Route::get('{any}', 'RoutingController@lk')->where('any', '.*');
