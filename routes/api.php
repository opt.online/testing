<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/register', 'Auth\RegisterController@register');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/refresh', 'Auth\LoginController@refresh');


Route::middleware('auth:api')->group( function () {

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('logout', 'AuthController@logout');


    //Route::get('/v1/category/list', 'CategoriesController@list');

    //Route::apiResource('/v1/getMenu', 'API\v1\MenuController');
    Route::get('/v1/products/create', 'ProductsController@createApi');
    Route::post('/v1/products/store', 'ProductsController@store');


    Route::post('/v1/products/import','ProductsController@import');

});

Route::apiResource('/v1/getMenu', 'API\v1\MenuController');
Route::get('/v1/category/list', 'CategoriesController@list');
